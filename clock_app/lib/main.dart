import 'package:flutter/material.dart';
import 'package:clock_app/clock_body.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // The Root Widget
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Clock',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new Clock(),
    );
  }
}

// The Clock
class Clock extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new ClockBody(),
          ],
        ),
      ),
    );
  }
}
